Neovim
******

My config from https://gitlab.com/pkimber/cfg

Neovim config from *TJ DeVries Advent of Neovim 2024 - December 2024*
https://www.youtube.com/watch?v=TQn2hJeHQbM

- https://www.pkimber.net/howto/vim/install/build.html
- https://www.pkimber.net/howto/vim/install/config.html
