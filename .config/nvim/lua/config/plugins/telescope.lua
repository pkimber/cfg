return {
  {
    "nvim-telescope/telescope.nvim",
    tag = '0.1.8',
    dependencies = {
      "nvim-lua/plenary.nvim",
      { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' }
    },
    config = function()
      require("telescope").setup {
        pickers = {
          buffers = {
            theme = "ivy"
          },
          find_files = {
            theme = "ivy"
          },
          grep_string = {
            theme = "ivy"
          },
          help_tags = {
            theme = "ivy"
          },
          quickfix = {
            theme = "ivy"
          },
        },
        extensions = {
          fzf = {}
        }
      }

      require("telescope").load_extension("fzf")

      vim.keymap.set("n", "<leader>en", function()
        require("telescope.builtin").find_files {
          cwd = vim.fn.stdpath("config")
        }
      end)
      vim.keymap.set("n", "<leader>fb", require("telescope.builtin").buffers)
      vim.keymap.set("n", "<leader>fd", require("telescope.builtin").find_files)
      vim.keymap.set("n", "<leader>fg", require("telescope.builtin").grep_string)
      vim.keymap.set("n", "<leader>fh", require("telescope.builtin").help_tags)
      vim.keymap.set("n", "<leader>fq", require("telescope.builtin").quickfix)
    end
  }
}
