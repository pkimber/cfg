return {
  {
    'saghen/blink.cmp',
    enabled = false,
    -- optional: provides snippets for the snippet source
    dependencies = 'rafamadriz/friendly-snippets',
    version = '*',
    opts = {
      keymap = { preset = 'default' },
      appearance = {
        use_nvim_cmp_as_default = true,
        nerd_font_variant = 'mono'
      },
      -- completion = {
      --   menu = { auto_show = function(ctx) return ctx.mode ~= 'cmdline' end }
      -- },
      signature = { enabled = true }
    },
  }
}
