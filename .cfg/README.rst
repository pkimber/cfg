Config / dotfile
****************

This method of looking after dot files works by creating a ``config`` alias for
the ``git`` command.

- The best way to store your dotfiles: A bare Git repository:
  https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
- YouTube Video
  https://www.youtube.com/watch?v=tBoLDpTWVOM&t=3s

Initial Setup
=============

.. note:: We only do this once *ever*!

::

  # using the fish shell
  fish
  # you just need to clone this after it is created
  # git init --bare $HOME/.cfg

New Workstation
===============

dotfiles::

  git clone --bare git@gitlab.com:pkimber/cfg.git ~/.cfg
  # create an alias for your fish shell
  # (just run the next three commands from your fish shell prompt)
  alias config="/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME"
  funcsave config
  config config --local status.showUntrackedFiles no

On a new laptop, ``config status`` will display the files as ``deleted``.
To restore the files::

  # list the *deleted* files
  config status
  # will not overwrite existing files
  config checkout

Usage
=====

Example commands::

  config status
  config add .vimrc
  config commit -m "Add vimrc"
  config add .bashrc
  config commit -m "Add bashrc"
  config push
